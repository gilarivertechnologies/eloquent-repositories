/** SQL file for testing */

CREATE TABLE IF NOT EXISTS `test_model_a` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `name` varchar(191) NOT NULL
);

CREATE TABLE IF NOT EXISTS `test_model_b` (
  `id` INTEGER PRIMARY KEY AUTOINCREMENT,
  `name` varchar(191) NOT NULL,
  `model_a_id` INTEGER
);