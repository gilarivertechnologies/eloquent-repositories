<?php
/**
 * Test ModelA
 */

namespace Nwilging\EloquentRepositories\Tests\TestModels;

use Nwilging\EloquentRepositories\Models\EloquentModelAbstract;

class TestModelA extends EloquentModelAbstract
{
    public $primaryKey = 'id';
    public $table = 'test_model_a';

    protected $fillable = ['name'];

    public $timestamps = false;

    public function testModelB()
    {
        return $this->hasOne(TestModelB::class, 'model_a_id');
    }
}