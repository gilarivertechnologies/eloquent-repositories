<?php
/**
 * Test ModelARepository
 */

namespace Nwilging\EloquentRepositories\Tests\TestRepositories;

use Nwilging\EloquentRepositories\Repositories\EloquentModelRepositoryAbstract;
use Nwilging\EloquentRepositories\Tests\TestModels\TestModelA;
use Psr\Log\LoggerInterface as LogContract;

class TestModelARepository extends EloquentModelRepositoryAbstract
{
    public function __construct(TestModelA $model, LogContract $log)
    {
        parent::__construct($model, $log);
    }
}