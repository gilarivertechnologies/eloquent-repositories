<?php
/**
 * Repository test for ModelA
 */

namespace Nwilging\EloquentRepositories\Tests\Integration;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Mockery\MockInterface;
use Nwilging\EloquentRepositories\Tests\TestCase;
use Nwilging\EloquentRepositories\Tests\TestModels\TestModelA;
use Nwilging\EloquentRepositories\Tests\TestModels\TestModelB;
use Nwilging\EloquentRepositories\Tests\TestRepositories\TestModelARepository;
use Psr\Log\LoggerInterface;

class ModelARepositoryTest extends TestCase
{
    /**
     * @var int
     */
    protected $modelById;

    /**
     * @var TestModelA
     */
    protected $modelByIdModel;

    /**
     * @var array
     */
    protected $modelsAll;

    /**
     * @var MockInterface
     */
    protected $log;

    /**
     * @var TestModelARepository
     */
    protected $repository;

    public function setUp()
    {
        parent::setUp();

        $modelById = new TestModelA(['name' => 'test model a']);
        $modelById->save();
        $this->modelByIdModel = $modelById;
        $this->modelById = $modelById->id;
        $this->modelsAll[] = $modelById;
        $this->log = \Mockery::mock(LoggerInterface::class);

        $this->repository = new TestModelARepository(new TestModelA(), $this->log);
    }

    protected function generateModels($count = 1, $name = null)
    {
        for($i=0;$i<$count;$i++)
        {
            $model = new TestModelA(['name' => ($name) ?: 'none']);
            $model->save();
            $this->modelsAll[] = $model;
        }
    }

    public function testFindById()
    {
        $model = $this->repository->findById($this->modelById);
        $this->assertInstanceOf(TestModelA::class, $model);
        $this->assertSame($this->modelById, $model->id);
    }

    public function testFindByIdOrFailSuccess()
    {
        $model = $this->repository->findByIdOrFail($this->modelById);
        $this->assertInstanceOf(TestModelA::class, $model);
        $this->assertSame($this->modelById, $model->id);
    }

    public function testFindByIdOrFailThrowsException()
    {
        $this->expectException(ModelNotFoundException::class);
        $this->repository->findByIdOrFail(999);
    }

    public function testFindAll()
    {
        $this->generateModels(5);
        $models = $this->repository->findAll();
        $this->assertInstanceOf(Collection::class, $models);
        $this->assertInstanceOf(TestModelA::class, $models->first());
        $this->assertTrue(in_array($models->first()->id, array_pluck($this->modelsAll, 'id')));
    }

    public function testCreate()
    {
        $this->log->shouldReceive('info')->once();
        $model = $this->repository->create(['name' => 'test created model']);
        $this->assertInstanceOf(TestModelA::class, $model);
        $this->assertDatabaseHas('test_model_a', $model->toArray());
    }

    public function testUpdate()
    {
        $this->log->shouldReceive('info')->once();
        $model = TestModelA::create(['name' => 'test model for update']);
        $update = ['name' => 'model updated!'];
        $updated = $this->repository->update($model, $update);
        $this->assertInstanceOf(TestModelA::class, $updated);
        $this->assertDatabaseHas('test_model_a', $update);
    }

    public function testDelete()
    {
        $this->log->shouldReceive('info')->once();
        $model = TestModelA::create(['name' => 'model to be deleted']);
        $this->repository->delete($model);
        $this->assertDatabaseMissing('test_model_a', $model->toArray());
    }

    public function testFindAllWhere()
    {
        $this->generateModels(5, 'test models with where condition');
        $this->generateModels(5, 'other models');
        $models = $this->repository->findAll([
            ['name', '=', 'test models with where condition']
        ]);
        $this->assertInstanceOf(Collection::class, $models);
        $this->assertCount(5, $models);
    }

    public function testFindAllWith()
    {
        $a = TestModelA::create(['name' => 'model with a relationship']);
        $b = TestModelB::create(['name' => 'model in a relationship']);
        $a->testModelB()->save($b);
        $models = $this->repository->findAll([], ['testModelB']);
        $this->assertInstanceOf(Collection::class, $models);
        $this->assertTrue(in_array($b->toArray(), array_pluck($models->toArray(), 'test_model_b')));
    }

    public function testFindAllLimit()
    {
        $this->generateModels(10);
        $models = $this->repository->findAll([], [], 5);
        $this->assertInstanceOf(Collection::class, $models);
        $this->assertCount(5, $models);
    }

    public function testFindAllWhereWithLimit()
    {
        $this->generateModels(5, 'test model');
        $this->generateModels(5, 'other models');
        $a = TestModelA::create(['name' => 'test model']);
        $b = TestModelB::create(['name' => 'model in a relationship', 'model_a_id' => $a->id]);

        $models = $this->repository->findAll(
            [
                ['name', '=', 'test model']
            ],
            ['testModelB'],
            6
        );

        $this->assertInstanceOf(Collection::class, $models);
        $this->assertCount(6, $models);
        $this->assertTrue(in_array($b->toArray(), array_pluck($models->toArray(), 'test_model_b')));
    }

    public function testFindByIdWith()
    {
        $a = TestModelA::create(['name' => 'model with relationship']);
        $b = TestModelB::create(['name' => 'model in a relationship']);
        $a->testModelB()->save($b);

        $found = $this->repository->findById($a->id, ['testModelB']);
        $this->assertInstanceOf(TestModelA::class, $found);
        $this->assertInstanceOf(TestModelB::class, $found->testModelB);
    }
}