<?php
/**
 * Service provider for package
 */

namespace Nwilging\EloquentRepositories;

use Illuminate\Support\ServiceProvider;

/**
 * Class EloquentRepositoryServiceProvider
 * @package Nwilging\EloquentRepositories
 */
class EloquentRepositoryServiceProvider extends ServiceProvider
{
    /**
     * Boot the service provider
     */
    public function boot()
    {

    }
}